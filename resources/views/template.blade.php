<!DOCTYPE html>
<head>
<!-- Basic Page Needs -->
<meta charset="utf-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title>Nitimandala</title>
<link rel="stylesheet" href="{{Config::get('app.url')}}/public/css/bootstrap.min.css">
<link href="https://fortawesome.github.io/Font-Awesome/assets/font-awesome/css/font-awesome.css" rel="stylesheet">
<!-- style -->
<link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">
<link rel="stylesheet" href="{{Config::get('app.url')}}/public/css/dd.css">
<link rel="stylesheet" type="text/css" href="{{Config::get('app.url')}}/public/css/slidermug.css">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/popup.css') }}">

<!-- SCRIPT -->
<!-- <script src="{{Config::get('app.url')}}/public/js/mugslider.js"</script> -->
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
</head>
<body>

<!-- 
<hr style="background-color: #999999!important; border: solid 3px #999999!important; height: 5px!important"></div> -->
<div class="kosong"></div>
<!-- THEY SAID -->
<div class="container">
<div class="row text-center">
<h3 style="font-size: 20px!important; font-weight: bold; font-style: italic; color: #172741!important;">Apa Kata Mereka</h3>
<div class="kosong"></div>
<div class="col-md-3 col-sm-3">
    <p>
        Kemarin suaminku ulang tahun, jadi bingung mau kasi apa. Untung ada temen yang rekomendasi untuk buat hadiah di Nitimandala. Pilihannya beragam, hasilnya juga bagus. Jadi puas.
    </p>
    <img src="{{Config::get('app.url')}}/public/img/icon/osas.png" class="img-circle bunder" alt="image"/>
    <h3>Intan (Pegawai Swasta)</h3>
</div>
<div class="col-md-3 col-sm-3">
    <p>
        Pusing mau kasi hadiah apa, buat temenku yang ulang tahun. Untung ada Nitimandala. Padahal aku gak bisa design, tapi karena editor toolnya gampang banget. Aku jadi gak pusing lagi. Terima kasih Nitimandala.
    </p>
    <img src="{{Config::get('app.url')}}/public/img/icon/osas.png" class="img-circle bunder" alt="image"/>
    <h3>Nikita Tjandra (Pelajar)</h3>
</div>
<div class="col-md-3 col-sm-3">
    <p>
        Semua yang aku perlukan ada di Nitimandala. Jadi lebih mudah mewujudkan semua karya-karyaku. Pokoknya puas !
    </p>
    <img src="{{Config::get('app.url')}}/public/img/icon/osas.png" class="img-circle bunder" alt="image"/>
    <h3>Louis Firman (Designer Graphics)</h3>
</div>
<div class="col-md-3 col-sm-3">
    <p>
        Puas banget dengan design dan hasil print di Niti. Dengna produk custom yang beragam, aku bisa merubah foto menjadi hasil print yang berkualitas tinggi dan dikemas sangat menarik didukung dengan teknologi print yang canggih. Servicenya juga sangat cepat dan mudah !
    </p>
    <img src="{{Config::get('app.url')}}/public/img/icon/osas.png" class="img-circle bunder" alt="image"/>
    <h3>Yuda (CEO Canda Photography)</h3>
</div>
</div>
</div>
<!-- THEY SAID ENDS HERE -->
<div class="kosong"></div>
<!--footer start from here-->
<footer class="footer-bs">
<div class="row">
<div class="col-md-3 footer-brand animated fadeInLeft">
<span style="font-weight: bold;font-size: 20px">NITIMANDALA</span><span>EDITOR</span>
<div class="kosong"></div>
<p style="font-size: 20px!important">www.nitimandala.com</p>
<hr>
<p style="font-size: 20px!important">www.editor/nitimandala.com</p>
</div>
<div class="col-md-4 footer-nav animated fadeInUp">
<p style="font-size: 20px!important">Site</p>
<div class="col-md-6">
    <ul class="pages">
        <li>
            <a href="#">Tentang Kami</a>
        </li>
        <li>
            <a href="#">Hubungi Kami</a>
        </li>
        <li>
            <a href="#">FAQ</a>
        </li>
        <li>
            <a href="#">Syarat & Ketentuan</a>
        </li>
        <li>
            <a href="#">Mobile Apps</a>
        </li>
    </ul>
</div>
<div class="col-md-6">
    <ul class="pages">
        <li>
            <a href="#">Blog</a>
        </li>
        <li>
            <a href="#">Privasi</a>
        </li>
        <li>
            <a href="#">Harga</a>
        </li>
        <li>
            <a href="#">Konfirmasi Pembayaran</a>
        </li>
    </ul>
</div>
</div>
<div class="col-md-2 footer-social animated fadeInDown">
<p style="font-size: 20px!important">Contact Us</p>
<a href="#"><i class="fa fa-instagram fa-2x jarakicon" aria-hidden="true"></i></a>
<a href="#"><i class="fa fa-facebook fa-2x jarakicon" aria-hidden="true"></i></a>
<a href="#"><i class="fa fa-twitter fa-2x jarakicon" aria-hidden="true"></i></a>
<a href="#"><i class="fa fa-google-plus fa-2x jarakicon" aria-hidden="true"></i></a>
<ul>
    <li>
        <a href="#"><i class="fa fa-phone" aria-hidden="true"></i> +62 89 609 277 640</a>
    </li>
    <li>
        <a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i> +62 89 609 277 640</a>
    </li>
    <li>
        <a href="#"><img src="{{ URL::asset('img/icon/line.png') }}" style="max-height: 20px;max-height: 20px;margin-left: -5px"> @NITIMANDALA</a>
    </li>
    <li>
        <a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i> <span style="font-size: 11px">INFO@NITIMANDALA.COM</span></a>
    </li>
</ul>
</div>
<div class="col-md-3 footer-ns animated fadeInRight">
<p>
    Sign up to our newsletter
</p>
<p>
    Enter your email address for news updates, offers and inspiration from us.
</p>
<p>
    <div class="input-group">
        <input type="text" class="form-control" placeholder="Enter your email address">
        <a class="btn btn-success" href="#" style="background-color: #999999!important; border-color: #999999!important; margin-top: 10px!important; color: #000000">SUBSCRIBE</a>
        <!-- <span class="input-group-btn">
                        <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-envelope"></span></button>
                      </span> -->
    </div>
    <!-- /input-group -->
</p>
</div>
</div>
</footer>
</div>
<script type="text/javascript">
    
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].onclick = function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  }
}
</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</body>
</html>
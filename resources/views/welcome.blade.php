@extends('template')
@section('content')


<div class="container-fluid">
    <div class="row" style="background-color: #172741!important">
        <div class="topnav col-md-4" style="background-color: #172741!important">
            <img src="{{Config::get('app.url')}}/public/img/icon/truk.png" class="headericon"><span style="color: #ffffff">Estimasi biaya pengiriman. <span><a href=""><span style="color: #ffffff; text-decoration: underline;">Selengkapnya</span></a>
        </div>

        <div class="col-md-8" style="padding-left: 650px">
                <img src="img/icon/fbbulet.png" style="max-height: 30px;max-width: 30px;margin-top: 3px">
                <img src="img/icon/wa.png" style="max-height: 30px;max-width: 30px;margin-top: 3px">
                <img src="img/icon/twitbulet.png" style="max-height: 30px;max-width: 30px;margin-top: 3px">
                <img src="img/icon/gplusbulet.png" style="max-height: 30px;max-width: 30px;margin-top: 3px">
                <img src="img/icon/igbulet.png" style="max-height: 30px;max-width: 30px;margin-top: 3px">
                <img src="img/icon/youtubebulet.png" style="max-height: 30px;max-width: 30px;margin-top: 3px">
        </div>
    </div>
</div>
<div class="row">
    <nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
            <ul class="nav navbar-nav">
                <li>
                    <a href=""><img src="{{Config::get('app.url')}}/public/img/logo.png" class="active" href="#home"/></a>
                </li>
                <li class="dropdown mega-dropdown menubar">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="font-size: 20px!important; font-weight: bold; font-style: italic; color: #172741!important;">Product<span class="caret"></span></a>
                    <div class="dropdown-menu mega-dropdown-menu" style="padding-bottom: 0px!important">
                        <div class="container-fluid">
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="men">
                                    <ul class="nav-list list-inline">
                                        <li>
                                            <a href="#"><img src="{{Config::get('app.url')}}/public/img/menu/x_banner.png" class="picmenu"><span>X Banner</span></a>
                                        </li>
                                        <li>
                                            <a href="#"><img src="{{Config::get('app.url')}}/public/img/menu/mug.jpg" class="picmenu"><span>Mug</span></a>
                                        </li>
                                        <li>
                                            <a href="#"><img src="{{Config::get('app.url')}}/public/img/menu/book.jpg" class="picmenu"><span>Album</span></a>
                                        </li>
                                        <li>
                                            <a href="#"><img src="{{Config::get('app.url')}}/public/img/menu/acrylic.jpg" class="picmenu"><span>Photo Acrylic</span></a>
                                        </li>
                                        <li>
                                            <a href="#"><img src="{{Config::get('app.url')}}/public/img/menu/canvas.jpg" class="picmenu"><span>Canvas</span></a>
                                        </li>
                                        <li>
                                            <a href="#"><img src="{{Config::get('app.url')}}/public/img/menu/flyers.png" class="picmenu"><span>Flyers</span></a>
                                        </li>
                                        <li>
                                            <a href="#"><img src="{{Config::get('app.url')}}/public/img/menu/kalender.jpg" class="picmenu"><span>Calendar</span></a>
                                        </li>
                                    </ul>
                                    <div class="row" style="background-color: #172741;padding-bottom: 0px!important;margin-bottom: 0px!important">
                                        <span style="color: #ffffff">All Product</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="menubar">
                    <a href="#" style="font-size: 20px!important; font-weight: bold; font-style: italic; color: #172741!important;">How to Order</a>
                </li>
                <li class="menubar">
                    <a href="#" style="font-size: 20px!important; font-weight: bold; font-style: italic; color: #172741!important;">About Us</a>
                </li>
            </ul>
            <ul class="nav-list list-inline">
                <li style="float: right!important; text-align: right!important; width: auto;">
                    <a class="btn btn-success" href="#"><i class="fa fa-sign-in" aria-hidden="true"></i> Sign In</a>
                </li>
                <li style="float: right!important; text-align: right!important; width: auto; ">
                    <h4>0 ITEM</h4>
                </li>
                <li style="float: right!important; text-align: right!important; width: auto;">
                    <a href="#"><img src="{{Config::get('app.url')}}/public/img/icon/keranjang.png" style="max-width: 30px; max-height: 30px;"></a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
    </nav>
</div>

<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="item active">
            <img class="ukuran-slide" src="img/home/banner_3.jpg" alt="First slide">
            <div class="carousel-caption"></div>
        </div>
        <div class="item">
            <img class="ukuran-slide" src="img/home/banner_3.jpg">
            <div class="carousel-caption"></div>
        </div>
        <div class="item">
            <img class="ukuran-slide" src="img/home/banner_3.jpg" alt="Third slide">
            <div class="carousel-caption"></div>
        </div>
    </div>
    <!--  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a><a class="right carousel-control"
                        href="#carousel-example-generic" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a> -->
</div>
<div class="main-text hidden-xs">
    <div class="col-md-12 text-center">
        <div class="">
            <a class="btn btn-clear btn-lg btn-min-block" href="http://www.jquery2dotnet.com/">Shop Now</a>
        </div>
    </div>
</div>
 <div class="kosong"></div>
 <div class="container">
<div class="row text-center">
<div class="col-md-4 col-sm-4">
    <img src="img/icon/photo-camera.png" class="bunder" alt="image"/>
    <h4>Abadikan Setiap Momen Indahmu</h4>
    <p>
        Karena setiap momen bersamanya adalah hal yang paling membahagiakan.
    </p>
</div>
<div class="col-md-4 col-sm-4">
    <img src="img/icon/paint-brush.png" class="img-circle bunder" alt="image"/>
    <h4>Desain sesuai keinginanmu</h4>
    <p>
        Editor online dan kemudahan lainnya sudah tersedia. Lakukan yang terbaik.
    </p>
</div>
<div class="col-md-4 col-sm-4">
    <img src="img/icon/image.png" class="img-circle bunder" alt="image"/>
    <h4>Cetak Dengan Kualitas Terbaik</h4>
    <p>
        Wujudkan semua mimpi indahmu. Kami siap membantu.
    </p>
</div>
</div>
</div>
<div>
<hr style="background-color: #999999!important; border: solid 3px #999999!important; height: 5px!important"></div>
<div class="kosong"></div>
<div class="row text-center">
<h3 style="font-size: 20px!important; font-weight: bold; font-style: italic; color: #172741!important;">Choose Your Gift</h3>
</div>
<div class="kosong"></div>
<div class="container">
<div id="products" class="row list-group">
<div class="item col-xs-3 col-lg-3">
    <div class="thumbnail">
        <img class="group list-group-image kotak-dalam" src="img/home/pic1.jpg" alt=""/>
        <div class="caption">
            <div class="row">
                <div class="col-xs-12 col-md-7">
                    <h4 class="group inner list-group-item-heading">Photo Print</h4>
                    <p class="pricing">Start From 100.000</p>
                </div>
                <div class="col-xs-12 col-md-5">
                    <a class="btn btn-success" href="http://www.jquery2dotnet.com">Shop Now</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="item col-xs-3 col-lg-3">
    <div class="thumbnail">
        <img class="group list-group-image kotak-dalam" src="img/home/pic2.jpg" alt=""/>
        <div class="caption">
            <div class="row">
                <div class="col-xs-12 col-md-7">
                    <h4 class="group inner list-group-item-heading">Album</h4>
                    <p class="pricing">Start From 470.000</p>
                </div>
                <div class="col-xs-12 col-md-5">
                    <a class="btn btn-success" href="http://www.jquery2dotnet.com">Shop Now</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="item col-xs-3 col-lg-3">
    <div class="thumbnail">
        <img class="group list-group-image kotak-dalam" src="img/home/pic1.jpg" alt=""/>
        <div class="caption">
            <div class="row">
                <div class="col-xs-12 col-md-7">
                    <h4 class="group inner list-group-item-heading">Canvas Print</h4>
                    <p class="pricing">Start From 200.000</p>
                </div>
                <div class="col-xs-12 col-md-5">
                    <a class="btn btn-success" href="http://www.jquery2dotnet.com">Shop Now</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="item col-xs-3 col-lg-3">
    <div class="thumbnail">
        <img class="group list-group-image kotak-dalam" src="img/home/pic2.jpg" alt=""/>
        <div class="caption">
            <div class="row">
                <div class="col-xs-12 col-md-7">
                    <h4 class="group inner list-group-item-heading">Merchandise</h4>
                    <p class="pricing">Start From 25.000</p>
                </div>
                <div class="col-xs-12 col-md-5">
                    <a class="btn btn-success" href="http://www.jquery2dotnet.com">Shop Now</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="item col-xs-3 col-lg-3">
    <div class="thumbnail">
        <img class="group list-group-image kotak-dalam" src="img/home/pic1.jpg" alt=""/>
        <div class="caption">
            <div class="row">
                <div class="col-xs-12 col-md-7">
                    <h4 class="group inner list-group-item-heading">Photo Print</h4>
                    <p class="pricing">Start From 100.000</p>
                </div>
                <div class="col-xs-12 col-md-5">
                    <a class="btn btn-success" href="http://www.jquery2dotnet.com">Shop Now</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="item col-xs-3 col-lg-3">
    <div class="thumbnail">
        <img class="group list-group-image kotak-dalam" src="img/home/pic1.jpg" alt=""/>
        <div class="caption">
            <div class="row">
                <div class="col-xs-12 col-md-7">
                    <h4 class="group inner list-group-item-heading">Photo Print</h4>
                    <p class="pricing">Start From 100.000</p>
                </div>
                <div class="col-xs-12 col-md-5">
                    <a class="btn btn-success" href="http://www.jquery2dotnet.com">Shop Now</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="item col-xs-3 col-lg-3">
    <div class="thumbnail">
        <img class="group list-group-image kotak-dalam" src="img/home/pic1.jpg" alt=""/>
        <div class="caption">
            <div class="row">
                <div class="col-xs-12 col-md-7">
                    <h4 class="group inner list-group-item-heading">Photo Print</h4>
                    <p class="pricing">Start From 100.000</p>
                </div>
                <div class="col-xs-12 col-md-5">
                    <a class="btn btn-success" href="http://www.jquery2dotnet.com">Shop Now</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="item col-xs-3 col-lg-3">
    <div class="thumbnail">
        <img class="group list-group-image kotak-dalam" src="img/home/pic1.jpg" alt=""/>
        <div class="caption">
            <div class="row">
                <div class="col-xs-12 col-md-7">
                    <h4 class="group inner list-group-item-heading">Photo Print</h4>
                    <p class="pricing">Start From 100.000</p>
                </div>
                <div class="col-xs-12 col-md-5">
                    <a class="btn btn-success" href="http://www.jquery2dotnet.com">Shop Now</a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<div>